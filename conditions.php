<?php

/**
 * But : étudier les différentes structures conditionnelles
 * 
 * Les documentations PHP se situent aux adresses suivantes :
 * - http://php.net/manual/fr/language.control-structures.php
 * - ...
 */

$a = 5;
$b = mt_rand(0,10); // mt_rand (cf http://php.net/mt_rand) est une fonction qui génère un nombre aléatoire

?>
<h1>Structures de contrôle</h1>

<p>Comparaison entre $a=<?php echo $a; ?> et $b=<?php echo $b; ?></p>

<?php

/**
 * Exercice 1
 * 
 * Afficher "Plus grand" si $b est strictement plus grand que $a
 */
echo '<p><b>Exercice 1</b></p>';

# code ici

/**
 * Exercice 2
 * 
 * Afficher
 * 	"Plus grand" si $b est strictement supérieur à $a
 *  "Plus petit" si $b est strictement inférieur à $a
 *  "Egaux" si $b est égal à $a
 */
echo '<p><b>Exercice 2</b></p>';

# code ici 

/**
 * Exercice 3
 * 
 * Utiliser l'opérateur ternaire pour réaliser la comparaison
 * Afficher 
 *  "Plus grand" si $b est strictement supérieur à $a
 *  "Plus petit ou égal" sinon
 */
echo '<p><b>Exercice 3</b></p>';

echo '<p>';
# code ici
echo '</p>';

/**
 * Exercice 4
 * 
 * Utiliser l'opérateur swtich pour afficher en lettre les chiffres de 
 * 1 à 3 et afficher "inconnu" pour tous les autres nombres
 */

echo '<p><b>Exercice 4</b></p>';

echo '<p>';
# code ici
echo '</p>';