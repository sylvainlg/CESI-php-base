<h1>Exercices sur les variables</h1>
<h2>Les variables simples</h2>
<pre>
<?php

// Variable simple

$a = 'la lettre a';
echo $a . "\n";

$a = 'ma variable a';
echo $a . "\n";

$a = $a . 'est ici';
echo $a . "\n";

$b = 'l\'autre variable';
echo $b . "\n";

?>

</pre>
<h2>Les tableaux</h2>
<pre>

<?php

/*
 * Déclarer un tableau
 * 
 * http://php.net/manual/en/language.types.array.php
 */

// Exemple de déclaration de tableau contenant des choses que vous pouvez faire avec PHP
$tableau = array(
	"Echanger avec les bases de données",
	"Envoyer des cookies", 
	"Evaluer les données d'un formulaire",
	"Fabriquer des pages Web dynamiques"
);

/*
 * La fonction var_dump() permet d'afficher le type et le contenu de n'importe quelle variable.
 * Elle est utile pour le debug.
 * Doc : http://php.net/manual/fr/function.var-dump.php
 * 
 * On notera que celle-ci affiche une erreur de type Notice 
 * tant que le tableau ci-dessus n'est pas créé.
 */
var_dump($tableau);

// Afficher la première valeur du tableau
echo '<p><b>Premier élément</b></p>';
# code ici

// Ajouter une phrase à la liste

# code ici

// Afficher le nouveau tableau
echo '<p><b>Nouvel élément</b></p>';

# code ici

// Afficher le nouvel élément (dernier élément)
// Utilisez la fonction http://php.net/count pour découvrir automatiquement la dernière clé
echo '<p><b>Dernier élément</b></p>';

#code ici


// Afficher la liste en HTML
// L'operateur http://php.net/foreach vous y aidera

# code ici


?>
</pre>

<h2>Les tableaux associatifs</h2>
<pre>

<?php

/*
 * Déclarer un tableau associatif
 * 
 * http://php.net/manual/en/language.types.array.php
 */

$tableau = [
	'a' => 'abricot,ananas',
	'p' => 'pêche,prune',
	'c' => 'cerise',
	'm' => 'mûre',
];
var_dump($tableau);

// Afficher le tableau sous forme d'une liste HTML en affichant [cle] : [valeur] derrière les puces

# code ici

// Récupérer l'ensemble des fruits dans un tableau simple (1 case par fruit)
// Aidez vous de la fonction explode

$fruits = [];
# code ici

var_dump($fruits);

// Trier le tableau
echo '<p><b>Tableau trié</b></p>';

# code ici

var_dump($fruits);

// Supprimer les fruits en "a" du tableau initial
echo '<p><b>Suppression des fruits en "a"</b></p>';
# code ici

var_dump($tableau);
?>
</pre>