<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <title>Ma page PHP</title>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    </head>

    <body>
       <h1>
		   <?php
		   # Afficher Bonjour [nom] ! si un nom est passé
		   # en paramètre GET, sinon afficher
		   # Bonjour inconnu !
		   ?>
       </h1>

	   <p>Changer mon nom</p>

	   <!-- écrire un formulaire permettant de récupérer le nom 
	   d'un utilisateur -->

		<p><a href="post.php">Recharger la page</a></p>
    </body>
</html>

