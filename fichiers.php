<?php

// Ouvrir le fichier data/35288-STMALO-Qualite_eaux_de_baignade.csv en lecture
// source : https://www.data.gouv.fr/fr/datasets/qualite-des-eaux-de-baignade-sur-le-littoral-de-saint-malo/

# code ici

// Lire le fichier et afficher chaque ligne dans un <p>

# code ici

// Utiliser les fonctions spécifiques aux csv afin d'afficher un tableau HTML 
// contenant l'ensemble des données contenues dans le fichier

# code ici

// Fermer le fichier

# code ici

// Ouvrir le fichier, en écriture cette fois

# code ici

// Rajouter une ligne au fichier afin d'enregistrer une nouvelle plage avec une note de Bon pour l'année année 2014

# code ici

// Enregistrer et fermer le fichier

# code ici


// Bonus : essayer d'extraire des données pertinentes du fichier data/station_osm_44.csv

