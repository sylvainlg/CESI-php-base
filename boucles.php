<?php

/**
 * But : étudier les différentes structures d'itération
 * 
 * La documentation PHP se situe à l'adresse http://php.net/manual/fr/language.control-structures.php
 */

?>

<h1>Boucles</h1>

<?php
/*

Ecrire le code réalisant l’output suivant avec les opérateurs :
 - for
 - do ... while
 - while

 +++++++
 ++++++
 +++++
 ++++
 +++
 ++
 +

*/

echo '<pre>';

# for

echo "\n\n";

# do ... while

echo "\n\n";

# while

echo '</pre>';


/*

 Ecrire le code réalisant le pattern suivant

 1******
 12*****
 123****
 1234***
 12345**
 123456*
 1234567

*/


echo '<pre>';

# code ici

echo '</pre>';



/*
 
Ecrire, sous forme d'une fonction, le code permettant de calculer la factorielle d'un nombre 

 - Ecrire une version récursive
 - Ecrire une version itérative

Tip : http://php.net/manual/fr/functions.user-defined.php
 */

function factorielle($n) {
	# code ici
	return 0;
}

echo '<pre>';
for($i=0; $i<10; ++$i) {
	echo 'La factorielle de ' . $i . ' est ' . factorielle($i) . "\n";	
}

function factorielle_iterative($n) {
	# code ici
	return 0;
}

$n = 6;
echo 'La factorielle de ' . $n . ' est ' . factorielle_iterative($n) . "\n";

echo '</pre>';